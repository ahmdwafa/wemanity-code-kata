﻿using System.Collections.Generic;

namespace csharp
{
    public class GildedRose
    {
        IList<Item> Items;
        public GildedRose(IList<Item> items)
        {
            Items = items;
        }

        public void UpdateQuality()
        {
            for (var i = 0; i < Items.Count; i++)
            {
                var item = Items[i];

                // "Sulfuras" >> Never sold, never change quality (80)
                if (item.Name == "Sulfuras, Hand of Ragnaros")
                {
                    item.Quality = 80;
                    Items[i] = item;
                    continue;
                }

                item.SellIn--;

                #region Quality Increase
                // "Aged Brie" >> Increase quality
                if (item.Name == "Aged Brie")
                {
                    item.Quality = IncreaseQuality(item.Quality, 1);
                    Items[i] = item;
                    continue;
                }

                // "Backstage passes" >> Increase quality by 1;
                // by 2 when SellIn <= 10 days;
                // by 3 when SellIn <= 5;
                // = 0 when SellIn has passed
                if (item.Name == "Backstage passes to a TAFKAL80ETC concert")
                {
                    if (item.SellIn <= 10)
                    {
                        if (item.SellIn < 0) // 0 when SellIn has passed
                        {
                            item.Quality = 0;
                        }
                        else if (item.SellIn <= 5) // by 3 when SellIn <= 5
                        {
                            item.Quality = IncreaseQuality(item.Quality, 3);
                        }
                        else // by 2 when SellIn <= 10 days
                        {
                            item.Quality = IncreaseQuality(item.Quality, 2);
                        }
                    }
                    else // Increase quality by 1
                    {
                        item.Quality = IncreaseQuality(item.Quality, 1);
                    }

                    Items[i] = item;
                    continue;
                }
                #endregion

                #region Quality Decrease
                // "Conjured" >> Quality decreases twice as fast
                if (item.Name == "Conjured Mana Cake")
                {
                    item.Quality = DegradeQuality(item.Quality, item.SellIn, 2);
                }
                // Any other item
                else
                {
                    item.Quality = DegradeQuality(item.Quality, item.SellIn, 1);
                }
                #endregion

                Items[i] = item;
            }
        }

        private int IncreaseQuality(int quality, int incrementBy)
        {
            var incrementedQuality = incrementBy + quality;
            return incrementedQuality >= 50 ? 50 : incrementedQuality;
        }

        private int DegradeQuality(int quality, int sellIn, int degradeBy)
        {
            if (sellIn < 0)
            {
                degradeBy *= 2;
            }

            var degradedQuality = quality - degradeBy;
            return degradedQuality <= 0 ? 0 : degradedQuality;
        }
    }
}

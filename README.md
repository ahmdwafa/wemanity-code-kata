# Wemanity Code Kata

Introduction:

This kata was originally downloaded from "https://github.com/T-rav/GildedRose-Refactoring-Kata". I added the technical requirements document as per the famous, described user story. I also fixed the failing unit tests by changing the lines splitting logic in "ApprovalTest.cs" and the wrongly put sequence of qualities for some items in "ThirtyDays.txt", for this specific fix please see this commit "https://bitbucket.org/ahmdwafa/wemanity-code-kata/commits/9b34d53be13b89da8d448ae2e1c060a50196a9c6".

How to run this code:

As simple as 1, 2, 3.. Clone the code, restore the NuGet packages (I didn't add neither remove any) and start finiding your way throughout the code. But I recommend you take a look at the couple of commits I made to have better understanding of my changes/additions.